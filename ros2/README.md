# A ROS 2 two-node talker and listener demo

This is a simple demo to package and deploy a simple two-node ROS application either:

- As a single application with two services (`talker-listener.app.yaml`), or
- As two applications (`listener.app.yaml` and `talker.app.yaml`) on a shared application network called `ros`.

This demo is built directly on [this example](https://docs.ros.org/en/foxy/How-To-Guides/Run-2-nodes-in-single-or-separate-docker-containers.html) and uses publicly available container images from [OSRF](https://hub.docker.com/r/osrf/ros/tags) on Docker Hub.

After you have deployed either the single- or two-application scenario, you should be able to look at the logs of `talker` or `listener` to confirm that they are able to talk to each other. The `listener` should log something like:

```text
[
  "[INFO] [1738241134.442462751] [listener]: I heard: [Hello World: 1420]",
  "[INFO] [1738241135.442436730] [listener]: I heard: [Hello World: 1421]",
  "[INFO] [1738241136.442500859] [listener]: I heard: [Hello World: 1422]",
  "[INFO] [1738241137.442465969] [listener]: I heard: [Hello World: 1423]",
  "[INFO] [1738241138.442563575] [listener]: I heard: [Hello World: 1424]"
]
```

## Building for ARM64 v8

We need an ARM64 v8 build in order to run this on our default demo AMIs. The `Containerfile` allows for building a an `arm64b8`-based image adding the `ros-jazzy-desktop` package that includes the demo applications.

Build like this:

```shell
podman build -t ros:jazzy-desktop-arm64 .
```

:warning: **Please note** that the resulting image is a whooping 4.11 GB in size

Tag and push to your liking (but change the URL to your own Control Tower):

```shell
podman tag ros:jazzy-desktop-arm64 registry.trial.cto.avassa.net/ros-jazzy-desktop
podman push registry.trial.cto.avassa.net/ros-jazzy-desktop
```
