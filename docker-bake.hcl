platforms = [ "linux/amd64", "linux/arm64" ]
alpine-vsn = "3.20"

function alpine-utils-dockerfile {
    params = [ packages ]
    result = "from alpine:${alpine-vsn}\nrun apk add --no-cache ${packages}\n"
}
function image-tag {
    params = [ image-name, version ]
    result = "registry.gitlab.com/avassa-public/application-examples/${image-name}:${version}"
}

target "utils" {
    matrix = {
        name = keys(targets)
    }
    name = name
    platforms = platforms
    dockerfile-inline = alpine-utils-dockerfile(targets[name].alpine-packages)
    tags = [ image-tag(name, targets[name].version) ]
    output = [ "type=registry" ]
}

targets = {
    nftables = {
        version = "1.1"
        alpine-packages = "nftables"
    }
    curl = {
        version = "1.0"
        alpine-packages = "curl"
    }
    openssh-client = {
        version = "1.0"
        alpine-packages = "openssh-client"
    }
    openssh-server = {
        version = "1.0"
        alpine-packages = "openssh-server"
    }
    iperf3 = {
        version = "1.0"
        alpine-packages = "iperf3"
    }
    socat = {
        version = "1.0"
        alpine-packages = "socat"
    }
    websocat = {
        version = "1.0"
        alpine-packages = "websocat"
    }
}
