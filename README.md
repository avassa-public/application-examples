# Application Examples
Collection of application examples.

## Naming convetion
- Application spec: `app-name.app.yaml`
- Vault: `vault-name.vault.yaml`
- Vault secret: `secret-name.vault-name.secret.yaml`
- CA: `ca-name.ca.yaml`
- CA Role: `ca-role-name.ca-name.ca-role.yaml`
