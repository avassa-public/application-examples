# Port mapping example

This example illustrates how to map a port outside the container to a different
port inside the container.

The application (python built-in http server) listens on port 8000 inside the
container. In the `network/ingress-ip-per-instance` section a TCP port 80 is
requested to be forwarded into the container, this is the port reachable
externally. An `init-container` runs before the main container and creates a
firewall rule that redirects all requests incoming to TCP port 80 to port 8000.
Note that the tenant must be allowed to use `net-admin` container capability
required to modify the firewall configuration. In order to do that make sure
that the `container-net-admin` capability is granted in the policy assigned
to the tenant.

The Dockerfile used to build the image for the `init-container` is trivial and
included here as `Dockerfile.nftables`.
